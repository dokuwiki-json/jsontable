jQuery(function() {
    //Verify, if json-plugin is installed. Initialize it, if necessary.
    if(typeof json_plugin !== 'object') {
        return;
    }
    if(!json_plugin.initialized) {
        json_plugin.init();
    }

    jQuery('.jsontable-plugin').each(function() {

        var $tabs = jQuery(this),
            $table = $tabs.find('.json-table'),
            options = $tabs.data('options'),
            saveall = $tabs.data('saveall'),
            errors = [],
            dataChanged = false,
            o = $tabs.data('o'),    //object with interface, which was prepared by json_plugin
            hot,
            dataOriginal;


        //prepare options and data for handsontable
        if(typeof options !== 'object') {
            try {
                options = JSON.parse(options);
            }
            catch(e) {
                errors.push("'options' attribute: " + String(e));
                options = {};
            }
        }
        else if (options === null) {
            options = {};
        }
        options.data = JSON.parse(o.data_combined);
        if(!options.hasOwnProperty('contextMenu')) {
            options.contextMenu = true;
        }


        //initialize handsontable
        hot = new Handsontable($table[0], options);


        //prepare search field
        if(options.hasOwnProperty('search')) {
            var $search = jQuery('<input type="search" placeholder="Search"/>').insertBefore($table);
            Handsontable.dom.addEvent($search[0], 'keyup', function (event) {
                hot.getPlugin('search').query(this.value);
                hot.render();
            }, hot);
        }


        //display errors
        if(errors.length) {
            $table.before('<div class="json-table-error">' + errors.join('<br/>') + '</div>');
        }


        //show 'save' button once after the  first change. Register
        //event the first time and each time, after json data are saved
        //(button hides after successfull save).
        function registerChangeEvent() {
            Handsontable.hooks.once('afterChange', function() {
                o.showButton();
                dataChanged = true;
            }, hot);
        }
        registerChangeEvent();
        o.$button.on('jsonAfterSave', registerChangeEvent);


        //write data into textarea as json string. From there data can be
        //saved to dokuwiki by json plugin.
        Handsontable.hooks.add('afterUnlisten', function () {
            if(dataChanged) {
                var dataToSave;

                if(saveall) {
                    dataToSave = options.data;
                }
                else {
                    if(dataOriginal === undefined) {
                        dataOriginal = JSON.parse(o.data_original);
                    }
                    dataToSave = json_plugin.diff(dataOriginal, options.data);
                }
                o.data_inline = JSON.stringify(dataToSave);
            }
        }, hot);
    });

});
