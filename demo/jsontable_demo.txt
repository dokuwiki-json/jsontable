====== JSON Table Demo ======

This is example for the [[https://www.dokuwiki.org/plugin:jsontable|JSON Table Plugin]].


===== Simple example =====
<code>
<jsontable id=cars path=cars options='{"colHeaders": ["Year", "Car", "Count"]}'>[
    ["2017", "Honda", 10],
    ["2018", "Toyota", 20],
    ["2019", "Nissan", 30]
]</jsontable>
</code>
<jsontable id=cars_ path=cars options='{"colHeaders": ["Year", "Car", "Count"]}'>[
    ["2017", "Honda", 10],
    ["2018", "Toyota", 20],
    ["2019", "Nissan", 30]
]</jsontable>


===== Advanced example =====

==== Table data definition ====
Add som data into JSON_database.cars:
<code>
<json path=cars2>[
    ["BMW",     2017, {"color": {"chassis": "white", "bumper": "white"}}],
    ["Renault", 2015, {"color": {"chassis": "yellow", "bumper": "black"}}],
    ["Tesla",   2018, {"color": {"chassis": "red"}}]
]</json>
</code>
<json path=cars2>[
    ["BMW",     2017, {"color": {"chassis": "white", "bumper": "white"}}],
    ["Renault", 2015, {"color": {"chassis": "yellow", "bumper": "black"}}],
    ["Tesla",   2018, {"color": {"chassis": "red"}}]
]</json>


==== Table options definition ====
Table options are basically JSON data and can be defined separately:
<code>
<json id=table_options_id path=table_options>{
    "colHeaders": ["Car", "Year", "Chassis color", "Bumper color"],
    "columns": [
        {},
        {"type": "numeric"},
        {
            "data": "2.color.chassis"
        },
        {
            "data": "2.color.bumper",
            "width": 100,
            "editor": "select",
            "selectOptions": ["yellow", "red", "black", "white"]
        }
    ],
    "search": true
}</json>
</code>
<json id=table_options__id path=table_options>{
    "colHeaders": ["Car", "Year", "Chassis color", "Bumper color"],
    "columns": [
        {},
        {"type": "numeric"},
        {
            "data": "2.color.chassis"
        },
        {
            "data": "2.color.bumper",
            "width": 100,
            "editor": "select",
            "selectOptions": ["yellow", "red", "black", "white"]
        }
    ],
    "search": true
}</json>


===== Table =====
%%<jsontable>%% element is first handled by [[https://www.dokuwiki.org/plugin:json|JSON Data Plugin]]. Attributes 'options' and 'save' are specific. Here is description for all elements:
  * ''id'' attribute is necessary, if we want to store changed data into dokuwiki document. This is handled by json plugin via safe ajax call.
  * ''path'' attribute sets data source for table to previously generated data in JSON_database.cars.
  * ''display'' attribute displays all tabs, just for info here.
  * ''//inline_data//'' are json data, which are combined with 'original_data'. 'original_data' are already defined on path by previous %%<json>%% elements or by 'src' attribute. We can say, 'inline_data' overrides 'original_data'. 'combined_data' are 'inline_data' combined with 'original_data' and is passed to the table as initial value. In the example below, chassis.color is set to green for second car. Please note, that here we access array elements with object notation.
  * ''save'' attribute specifies, how data are stored into ''//inline_data//''. If set to ''all'', then all 'combined_data' will be stored into 'inline_data' on table change. Otherwise only the difference form 'original data' will be saved. This is the default behavior.
  * ''options'' attribute is a JSON string directly passed into [[https://handsontable.com/docs/6.2.2/Options.html|Handsontable options]].

<code>
<jsontable id=cars2 path=cars2 display=all save=diff options=%$table_options%>
{"1":{"2":{"color":{"chassis":"green"}}}}
</jsontable>
</code>
<jsontable id=cars2_ path=cars2 display=all save=diff options=%$table_options%>
{"1":{"2":{"color":{"chassis":"green"}}}}
</jsontable>
